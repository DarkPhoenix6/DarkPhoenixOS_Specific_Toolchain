/* note these headers are all provided by newlib - you don't need to provide them */
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <sys/times.h>
#include <sys/errno.h>
#include <sys/time.h>
#include <stdio.h>
#include <syscall.h>
#include <errno.h>
#undef errno
extern int errno;
# define __set_errno(val) (errno = (val))

void _exit();
int close(int file);
char **environ; /* pointer to array of char * strings that define the current environment variables */
int execve(char *name, char **argv, char **env);
int fork();
int fstat(int file, struct stat *st);
int getpid();
int isatty(int file);
int kill(int pid, int sig);
int link(char *old, char *new);
int lseek(int file, int ptr, int dir);
int open(const char *name, int flags, ...);
int read(int file, char *ptr, int len);
caddr_t sbrk(int incr);
int stat(const char *file, struct stat *st);
clock_t times(struct tms *buf);
int unlink(char *name);
int wait(int *status);
int write(int file, char *ptr, int len);
int gettimeofday(struct timeval *__restrict __p,
			  void *__restrict __tz);


int close(int file) {
  return -1;
}

char *__env[1] = { 0 };
char **environ = __env;

/* 
#include <errno.h>
#undef errno
extern int errno;
*/
int execve(char *name, char **argv, char **env) {
  errno = ENOMEM;
  return -1;
}

/* 
#include <errno.h>
#undef errno
extern int errno;
*/
int fork(void) {
  errno = EAGAIN;
  return -1;
}

/*
#include <sys/stat.h>
*/

int fstat(int file, struct stat *st) {
  st->st_mode = S_IFCHR;
  return 0;
}

int isatty(int file) {
  return 1;
}

/* 
#include <errno.h>
#undef errno
extern int errno;
*/
int kill(int pid, int sig) {
  errno = EINVAL;
  return -1;
}

/* 
#include <errno.h>
#undef errno
extern int errno;
*/

int link(char *old, char *new) {
  errno = EMLINK;
  return -1;
}

int open(const char *name, int flags, ...) {
//int open(const char *name, int flags, int mode) {
  return -1;
}

int read(int file, char *ptr, int len) {
  return 0;
}


caddr_t sbrk(int incr) {
  extern char _end;		/* Defined by the linker */
  static char *heap_end;
  char *prev_heap_end;
  register char *stack_ptr asm ("esp");
 
  if (heap_end == 0) {
    heap_end = &_end;
  }
  prev_heap_end = heap_end;
  if (heap_end + incr > stack_ptr) {
    write (1, "Heap and stack collision\n", 25);
    abort ();
  }

  heap_end += incr;
  return (caddr_t) prev_heap_end;
}


int stat(const char *file, struct stat *st) {
  st->st_mode = S_IFCHR;
  return 0;
}

clock_t times(struct tms *buf) {
  return -1;
}

/* 
#include <errno.h>
#undef errno
extern int errno;
*/
int unlink(char *name) {
  errno = ENOENT;
  return -1; 
}

/* 
#include <errno.h>
#undef errno
extern int errno;
*/

int wait(int *status) {
  errno = ECHILD;
  return -1;
}


int write(int file, char *ptr, int len) {
#undef USEFUL_DEFAULT
#ifdef USEFUL_DEFAULT
  int todo;

  for (todo = 0; todo < len; todo++) {
    outbyte (*ptr++);
  }
  return len;
#else
  int res; 
      asm volatile("int $0x80" : "=a" (res) : "0" (SYS_WRITE), "b" ((int)file), "c" ((long)ptr), "d" ((int)len));  
  return res;
#endif
}
